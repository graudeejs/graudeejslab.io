{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  buildInputs = [
    pkgs.ruby
    pkgs.nodejs-16_x
  ];
}
