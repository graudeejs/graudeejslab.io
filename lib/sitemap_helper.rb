include Nanoc::Helpers::XMLSitemap

class SitemapHelper

  def initialize(view)
    self.view = view
  end

  def build
    view.xml_sitemap(items: sitemap_items, rep_select: ->(rep) {
      rep.name == :default
    })
  end

  private

  attr_accessor :view

  def sitemap_items
    view.items.to_a.select do |item|
      include_in_sitemap?(item)
    end
  end

  def include_in_sitemap?(item)
    return false unless item.path
    return true if item[:sitemap]
    return false if item[:sitemap] == false
    item.identifier.without_ext.end_with?('.html')
  end
end
