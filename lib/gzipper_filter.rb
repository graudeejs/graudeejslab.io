class GzipperFilter < Nanoc::Filter
  require 'zlib'

  identifier :gzipper

  def run(content, params={})
    filename = File.basename(item.identifier.to_s)
    out = StringIO.new
    gz = Zlib::GzipWriter.new(out)
    gz.mtime = File.mtime(item.raw_filename)
    gz.orig_name = File.basename(filename)
    gz.write content
    gz.close

    out.string
  end
end
